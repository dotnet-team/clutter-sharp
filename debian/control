Source: clutter-sharp
Section: cli-mono
Priority: optional
Maintainer: Debian CLI Libraries Team <pkg-cli-libs-team@lists.alioth.debian.org>
Uploaders: Chow Loong Jin <hyperair@debian.org>
Build-Depends: debhelper (>= 7.0.50~),
               autotools-dev,
               dh-autoreconf,
               cli-common-dev (>= 0.5.7),
               mono-devel (>= 2.4.3),
               libgtk2.0-cil-dev (>= 2.12.1),
               libclutter-1.0-dev (>= 1.0.1),
               monodoc-base
Standards-Version: 3.9.2
Homepage: http://git.clutter-project.org/bindings/clutter-sharp/
Vcs-Git: git://git.debian.org/pkg-cli-libs/packages/clutter-sharp.git
Vcs-Browser: http://git.debian.org/?p=pkg-cli-libs/packages/clutter-sharp.git

Package: libclutter-cil
Architecture: all
Depends: ${cli:Depends},
         ${misc:Depends}
Description: Open GL based interactive canvas library -- CLI Bindings
 Open GL based interactive canvas library Clutter is an Open GL based
 interactive canvas library, designed for creating fast, mainly 2D single window
 applications such as media box UIs, presentations, kiosk style applications and
 so on.
 .
 This package provides CLI bindings to a build of Clutter that uses the GLX
 OpenGL backend.

Package: libclutter-cil-dev
Architecture: all
Depends: libclutter-cil (= ${binary:Version}),
         ${misc:Depends}
Description: Open GL based interactive canvas library -- CLI development files
 Open GL based interactive canvas library Clutter is an Open GL based
 interactive canvas library, designed for creating fast, mainly 2D single window
 applications such as media box UIs, presentations, kiosk style applications and
 so on.
 .
 This package provides development files used for compiling CLI applications
 which use this library.

Package: monodoc-clutter-manual
Architecture: all
Section: doc
Depends: monodoc-manual, ${misc:Depends}
Description: compiled XML documentation for clutter-sharp
 Clutter-Sharp is a set of CLI bindings for libclutter, which is an Open GL
 based interactive canvas library designed for creating fast, mainly 2D single
 window applications such as media box UIs, presentations, kiosk style
 applications, and so on.
 .
 This package contains the compiled XML documentation for clutter-sharp.
